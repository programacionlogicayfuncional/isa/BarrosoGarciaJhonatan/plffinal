(ns plffinal.core
  (:require [clojure.set :as conjunto]))



(defn count-simbolos
  ;c colección
  ;s símbolo
  [c s] (let [f (fn [p] (= p s))]
          (count (filter f c))))

(defn simbolos
  [c] (let [f {:oeste (count-simbolos c \<)
               :este (count-simbolos c \>)
               :sur (count-simbolos c \v)
               :norte (count-simbolos c \^)}] f))

((simbolos [\< \< \< \> \>]) :norte)

;Solución planteada al problema 1
(defn regresa-al-punto-de-origen?
  [c] (let [m (simbolos c)]
        (or (empty? c) (and (= (m :oeste) (m :este))
                            (= (m :norte) (m :sur))))))
(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))

(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])

;Solución al problema 2
;Dada n secuencias de sentidos indicar si todas regresan a su propio punto de origen:
(defn regresan-al-punto-de-origen? [& args]
  (let [r (map (fn [p] (regresa-al-punto-de-origen? p))
               (into [] args))
        c (into #{} r)] (not (contains? c false))))


(into #{} (map (fn [p] (regresa-al-punto-de-origen? p))
             [[\v \v \^ \^ \^] [\v \v \^ \^ \^]]))

(contains? #{false true} false)
(not (contains? #{false true} false))

;Pruebas del problema 2
(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))

(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])

;Problema 3
;Dada una secuencia de sentidos, regresar la secuencia de sentidos
; que permita regresar al punto de origen en sentido contrario:

(defn regreso-al-punto-de-origen [c]
  (let [m (simbolos c)
        h (map (fn [p] (cond
                         (= p \<) \>
                         (= p \>) \<
                         (= p \^) \v
                         :else \^)) (reverse c))
        g (fn[] (cond
                  (empty? c) ()
                  (and (= (m :oeste) (m :este)) (= (m :norte) (m :sur))) ()
                  :else h))]
    (g)))


(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))

(regreso-al-punto-de-origen ">>>")
(regreso-al-punto-de-origen [\< \v \v \v \> \>]) 


;Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar si ambas terminan 
;en el mismo punto final.


(defn mismo-punto-final?[s1, s2]
  (let [m1 (simbolos s1)
        m2 (simbolos s2)]
     (and (= (- (m1 :norte)(m1 :sur))  (- (m2 :norte) (m2 :sur))) 
          (= (- (m1 :oeste) (m1 :este)) (- (m2 :oeste) (m2 :este))))))

(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))

;Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen,
; indicar cuantas veces coinciden en un mismo punto:

(defn coincidencias[s1, s2]
  (let [h (fn [v, p] (cond
             (= p \^) {:x ((last v) :x) :y (inc ((last v) :y))}
             (= p \v) {:x ((last v) :x) :y (dec ((last v) :y))}
             (= p \>) {:x (inc ((last v) :x)) :y ((last v) :y)}
             (= p \<) {:x (dec ((last v) :x)) :y ((last v) :y)}))
        g (fn [s] (loop [xs s
                 result [{:x 0 :y 0}]]
            (if (> (count xs)0)
              (let [x (first xs)]
                (recur (next xs) (conj result (h result x))))
              result)))
        c1 (g s1)
        c2 (g s2)] 
   (count (conjunto/intersection (apply hash-set c1)
                                    (apply hash-set c2)) )))

(coincidencias "" [])
(coincidencias (list \< \<) [\> \>])

(coincidencias [\^ \> \> \> \^] ">^^<")
(coincidencias "<<vv>>^>>" "vv<^")
(coincidencias ">>>>>" [\> \> \> \> \>])
(coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))




