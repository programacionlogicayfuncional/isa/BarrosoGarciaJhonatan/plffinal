(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.core :refer :all]))

(deftest a-test
(testing "Problema 1 (regresa-al-punto-de-origen? " ") -> true"
           (is (= true (regresa-al-punto-de-origen? ""))))
(testing "Problema 1 (regresa-al-punto-de-origen? ><) -> true "
    (is (= true (regresa-al-punto-de-origen? "><"))))
  
(testing "Problema 1 (regresa-al-punto-de-origen? >) -> false "
           (is (= false (regresa-al-punto-de-origen? ">"))))
(testing "Problema 1 (regresa-al-punto-de-origen? <^) -> false "
  (is (= false (regresa-al-punto-de-origen? [\< \^]))))

(testing "Problema 2 (regresan-al-punto-de-origen?) -> true"
            (is (= true (regresan-al-punto-de-origen?))))
(testing "Problema 2 (regresan-al-punto-de-origen? []) -> true "
  (is (= true (regresan-al-punto-de-origen? []))))

(testing "Problema (regresan-al-punto-de-origen? (list < >) ^^ [> <])"
  (is (= false (regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<]))))
(testing "Problema (regresan-al-punto-de-origen? [< < > > > > > > > >]) "
  (is (= false (regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>]))))

(testing "Problema 4 (mismo-punto-final? "" []) -> true"
  (is (= true (mismo-punto-final? "" []))))
(testing "Problema 4 (mismo-punto-final? ^^^ <^^^>) -> true "
  (is (= true (mismo-punto-final? "^^^" "<^^^>"))))

(testing "Problema 4 (mismo-punto-final? "" <) -> false"
  (is (= false (mismo-punto-final? "" "<"))))
(testing "Problema 4 (mismo-punto-final? >>  <>) -> false"
  (is (= false (mismo-punto-final? [\> \>] "<>"))))
(testing "Problema 5 (coincidencias "" []) -> 1"
         (is (= 1 (coincidencias "" []))))
(testing "Problema 5 (coincidencias (list < <) [> >]) -> 1 "
  (is (= 1 (coincidencias (list \< \<) [\> \>]))))

(testing "Problema 5 (mismo-punto-final? " " <) -> false"
         (is (= false (mismo-punto-final? "" "<"))))
(testing "Problema 5 (coincidencias >>>>> [> > > > >]) -> 6"
  (is (= 6 (coincidencias ">>>>>" [\> \> \> \> \>]))))




)
