(defproject plffinal "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [clj-kondo "2020.12.09"]
                 [lambdaisland/kaocha "1.0.732"]
                 [lambdaisland/kaocha-cloverage "0.0-32"]]
  :plugins [[lein-cljfmt "0.7.0"]
            [jonase/eastwood "0.3.12"]
            [lein-kibit "0.1.8"]]
  :aliases {"clj-kondo" ["run" "-m" "clj-kondo.main" "--lint" "src:test"]
            "kaocha" ["run" "-m" "kaocha.runner" "--color" "--reporter" "documentation"]
            "cloverage" ["run" "-m" "kaocha.runner" "--plugin" "cloverage" "--no-cov-html" "--cov-summary"]}
  :repl-options {:init-ns plffinal.core})
